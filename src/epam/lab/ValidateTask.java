package epam.lab;

import org.apache.tools.ant.*;

import java.io.File;
import java.util.*;

public class ValidateTask extends Task {
    private boolean checkdepends;
    private boolean checkdefault;
    private boolean checknames;
    private List<ValidateFile> buildfiles = new ArrayList<>();

    public void setCheckdepends(boolean checkdepends) {
        this.checkdepends = checkdepends;
    }

    public void setCheckdefault(boolean checkdefault) {
        this.checkdefault = checkdefault;
    }

    public void setChecknames(boolean checknames) {
        this.checknames = checknames;
    }

    @Override
    public void execute() throws BuildException {
        for (ValidateFile buildFile : buildfiles) {
            File file = buildFile.getFile();
            Project project = (Project) getProjectForValidation(file);
            checkdefault(project);
            checknames(project);
            checkdepends(project);
            log("compile");
        }
    }

    private Project getProjectForValidation(final File file) {
        Project project = new Project();
        project.setUserProperty("ant.file", file.getAbsolutePath());
        project.init();
        ProjectHelper helper = ProjectHelper.getProjectHelper();
        project.addReference("ant.projectHelper", helper);
        helper.parse(project, file);
        return project;
    }

    private boolean checkdefault(Project project) {
        if (checknames) {
            String defaultTarget = project.getDefaultTarget();
            return defaultTarget != null;
        }
        return true;
    }

    private boolean checknames(Project project) {
        final String patName = "^[a-zA-Z-]*$";
        if (checknames) {
            Set<String> names = project.getTargets().keySet();
            return names.stream().allMatch(name -> name.matches(patName));
        }
        return true;
    }

    private boolean checkdepends(Project project) {
        if (checkdepends) {
            Hashtable<String, Target> targets = project.getTargets();
            if (targets.size() < 2) {
                return false;
            }

            for (Map.Entry<String, Target> entry : targets.entrySet()) {
                Target target = entry.getValue();
                Enumeration<String> dependencies = target.getDependencies();
                if (dependencies != null && dependencies.hasMoreElements()) {
                    return true;
                }
            }
            return false;
        }
        return true;
    }


}
