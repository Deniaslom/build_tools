package epam.lab;


import org.apache.tools.ant.Task;

import java.io.File;

public class ValidateFile extends Task {
    private File location;

    public ValidateFile() {
    }

    public final void setLocation(final File location) {
        this.location = location;
    }

    public File getFile() {
        return location;
    }


}